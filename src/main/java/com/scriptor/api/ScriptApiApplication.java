package com.scriptor.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ScriptApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(ScriptApiApplication.class, args);
    }
}
