package com.scriptor.api.controller;

import com.jaunt.JauntException;
import com.scriptor.api.service.ScriptService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ScriptControllerTest {
    public static final String MOVIE_NAME = "Batman";
    public static final String SCRIPT = "I am batman";

    @InjectMocks
    private ScriptController scriptController;
    @Mock
    private ScriptService scriptService;

    @Test
    void getScripts_success() throws JauntException {
        when(scriptService.getScripts(MOVIE_NAME)).thenReturn(SCRIPT);
        String actualScripts = scriptController.getScripts(MOVIE_NAME);
        assertEquals(SCRIPT, actualScripts);
    }
}
